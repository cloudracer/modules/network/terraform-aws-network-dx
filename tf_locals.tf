locals {
  tags_module = {
    Terraform               = true
    Terraform_Module        = "terraform-aws-network-dx"
    Terraform_Module_Source = "https://gitlab.com/tecracer-intern/terraform-landingzone/modules/terraform-aws-network-dx"
  }
  tags = merge(local.tags_module, var.tags)
}
