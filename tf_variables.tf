// General
variable "name" {
  description = "Name to be used on all the resources as identifier"
  type        = string
  default     = ""
}

variable "transit_gateway_id" {
  description = "VPC prefixes (CIDRs) to advertise to the Direct Connect gateway."
  type        = string
  default     = ""
}

variable "allowed_prefixes" {
  description = "The Autonomous System Number (ASN) of the Amazon side of the gateway"
  type        = list(string)
  default     = []
}

variable "amazon_side_asn" {
  description = "The Autonomous System Number (ASN) of the Amazon side of the gateway"
  type        = number
  default     = 65000
}

// Tags
variable "tags" {
  type        = map(string)
  description = "Tag that should be applied to all resources."
  default     = {}
}
