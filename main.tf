resource "aws_dx_gateway" "this" {
  name            = lower("dx-gateway-${lower(var.name)}-tflz")
  amazon_side_asn = var.amazon_side_asn
}

resource "aws_dx_gateway_association" "this" {
  dx_gateway_id         = aws_dx_gateway.this.id
  associated_gateway_id = var.transit_gateway_id

  allowed_prefixes = var.allowed_prefixes
}

data "aws_ec2_transit_gateway_dx_gateway_attachment" "this" {
  transit_gateway_id = var.transit_gateway_id
  dx_gateway_id      = aws_dx_gateway.this.id

  depends_on = [aws_dx_gateway_association.this]
}

resource "aws_ec2_transit_gateway_route_table" "this" {
  transit_gateway_id = var.transit_gateway_id

  tags = merge(var.tags, local.tags, {
    Name = lower("tgw-rtb-dx-tflz")
  })
}

resource "aws_ec2_transit_gateway_route_table_association" "this" {
  transit_gateway_attachment_id  = data.aws_ec2_transit_gateway_dx_gateway_attachment.this.id
  transit_gateway_route_table_id = aws_ec2_transit_gateway_route_table.this.id
}